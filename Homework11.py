from collections import UserDict
from datetime import datetime
import re
import json
import pickle
from os.path import exists
from os import remove


# AddressBook реализует метод iterator, который возвращает генератор по записям AddressBook и за одну итерацию возвращает представление для N записей.

class Field:
    _value = None

    def __init__(self):
        pass

    @property
    def value(self):
        return self._value


class Birthday(Field):

    def __init__(self, value: str):
        super().__init__()
        self.value: datetime = value

    @Field.value.setter
    def value(self, value):
        try:
            self._value = datetime.strptime(value, "%d-%m-%Y")
        except:
            print("Wrong format")


class Name(Field):

    def __init__(self, value):
        super().__init__()
        self._value = value

    def __repr__(self):
        return self._value


class Phone(Field):

    def __init__(self, value):
        super().__init__()
        self.value = value

    def __repr__(self):
        return self._value

    @Field.value.setter
    def value(self, value):
        try:
            if re.match(r"(?:\+\d{2})?\d{3,4}\D?\d{3}\D?\d{3}", value).group() == value:
                self._value = value
        except:
            print("Wrong format")


class Record:

    def __init__(self, name: Name, phones: list[Phone] = [], birthday: Birthday = None):
        self.name: Name = name
        self.phones: list[Phone] = phones
        self.birthday: Birthday = birthday

    def add_phone(self, phone: Phone):
        self.phones.append(phone)

    def remove_phone(self, phone_to_remove: Phone):
        for phone in self.phones:
            if phone.value == phone_to_remove.value:
                self.phones.remove(phone)

    def edit_phone(self, phone_to_edit: Phone, new_phone: Phone):
        self.remove_phone(phone_to_edit)
        self.add_phone(new_phone)

    def days_to_birthday(self):
        if self.birthday is None:
            print("There is no birthday date for this user")
            return

        current_birthday = datetime(year=datetime.now().year,
                                    month=self.birthday.value.month,
                                    day=self.birthday.value.day)
        if datetime.now().month >= current_birthday.month:
            if datetime.now().day >= current_birthday.day:
                current_birthday = datetime(year=current_birthday.year + 1,
                                            month=current_birthday.month,
                                            day=current_birthday.day)
        return (current_birthday - datetime.now()).days + 1


class AddressBook(UserDict):
    page = 0
    __items_per_page = 2
    to_return = None

    def __iter__(self):
        return self

    def __next__(self):
        records = list(self.data.items())
        start_index = self.page * self.__items_per_page
        end_index = (self.page + 1) * self.__items_per_page
        self.page += 1
        if self.page * self.__items_per_page >= len(records) + self.__items_per_page:
            raise StopIteration
        if len(records) >= end_index:
            self.to_return = records[start_index:end_index]
            return self.to_return
        else:
            self.to_return = records[start_index:-1]
            return [[record[1], record[0]] for record in self.to_return]

    def find_by_name(self, name: str):
        result = {}
        for user_name in self.data.keys():
            if name in user_name.value:
                result[user_name] = self.data[user_name]
        return result

    def find_by_phones(self, phone: str):
        result = {}
        b = self.data
        for record in self.data.items():
            for user_phone in record[1]:
                if phone in user_phone.value:
                    # result[record[0]] = self.data[record[1]]
                    result[record[0]] = self.data[record[0]]
        return result

    def add_record(self, record: Record):
        self.data[record.name] = record.phones

    def save_data(self):
        if exists('data'):
            remove('data')
        with open('data', mode='xb') as fh:
            pickle.dump(self, fh)

    @staticmethod
    def load_data():
        with open('data', mode='rb') as fh:
            return pickle.load(fh)



b = Birthday("5-09-1996")
r = Record(name=Name("Lesha"), phones=[Phone('+380672542885')], birthday=b)
r1 = Record(name=Name("Sasha"), phones=[Phone('+380672542886')], birthday=b)
print(r.days_to_birthday())
a = AddressBook()
a.add_record(r)
a.add_record(r1)
for el in a:
    print(el)

print(a.find_by_name("Le"))
print(a.find_by_phones("2884"))

a.save_data()

b = AddressBook().load_data()
print(b)
